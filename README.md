# Product API

Simple rest api to handle operations for products and images.

## Getting Started

Checkout the project on: git@bitbucket.org:brunozm/productapi.git

If you are using linux, just run: 

	git clone git@bitbucket.org:brunozm/productapi.git

This command will create a folder called 'productapi' in your active directory.

### Prerequisites

To run 'Product API' you must had install Maven, if you are using linux, run:

	mvn version

If printed something like this: Apache Maven 3.3.x, you be able to use the product api. :)

Otherwise, run 'sudo apt-get install maven' .

### Installing

You don't need install anything to use the product api, just be inside the folder of project (/productapi),

and run the command:

	mvn spring-boot:run

## Usage

Product api provides some services for product and image, as create, update and delete, for more

informations, access the Swagger documentation on: 

	http://localhost:8080/api/swagger.json
	
For a while, we have just the documentation api in json format, but I intend build an UI swagger 

documentation to be more usefull.

## Running the tests

To run the tests, you must run the command:

	mvn test

## Built With

* Spring Boot
* H2
* Hibernate
* Maven
* Swagger

## Author

* **Bruno Zaccaria Marques** - (https://bitbucket.org/brunozm/)


