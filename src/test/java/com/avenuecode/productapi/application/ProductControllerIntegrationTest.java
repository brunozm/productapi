package com.avenuecode.productapi.application;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.avenuecode.productapi.domain.image.Image;
import com.avenuecode.productapi.domain.product.Product;
import com.avenuecode.productapi.domain.product.ProductRepository;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations="classpath:test-application.properties")
public class ProductControllerIntegrationTest {
	
	@Autowired
	private TestRestTemplate restTemplate;
	
	@Autowired
	private ProductRepository productRepository;
	
	private Boolean initialDataLoaded = false;
	
	@Before
	public void init() {
		if (!initialDataLoaded) {
			loadInitialData();
		}
	}
	
	@Test
	public void mustSaveAProduct() {
		Product product = new Product("New test product");
		product.addImage(new Image("JPEG"));
		ResponseEntity<Product> result = restTemplate.postForEntity("/api/products", product, Product.class);
		
		Assert.assertEquals(HttpStatus.OK, result.getStatusCode());
		Assert.assertEquals(product.getDescription(), result.getBody().getDescription());
	}
	
	@Test
	public void mustUpdateAProductById() {
		Product firstProduct = productRepository.findAll().iterator().next();
		Integer id = firstProduct.getId();
		
		Product product = new Product("new description");
		product.addProducts(firstProduct.getProducts());
		product.addImages(firstProduct.getImages());
		
		firstProduct.setDescription("new description");
		restTemplate.put("/api/products/" + id, product);
		
		Product result = productRepository.findOne(id);
		
		Assert.assertEquals(product.getDescription(), result.getDescription());
	}
	
	@Test
	public void mustDeleteAProductById() {
		Product firstProduct = productRepository.findAll().iterator().next();
		Integer id = firstProduct.getId();
		
		restTemplate.delete("/api/products/" + id);
		
		Product result = productRepository.findOne(id);
		Assert.assertNull(result);
	}
	
	@Test
	public void mustListAllProducts() {
		ResponseEntity<String> result = restTemplate.getForEntity("/api/products", String.class);
		
		Map<String, Object> jsonFromFirstResult = jsonToListOfMap(result.getBody()).get(0);
		
		Assert.assertEquals(HttpStatus.OK, result.getStatusCode());
		Assert.assertTrue(jsonFromFirstResult.containsKey("products"));
		Assert.assertTrue(jsonFromFirstResult.containsKey("images"));
	}
	
	@Test
	public void mustListAllProductsAsBrief() {
		ResponseEntity<String> result = restTemplate.getForEntity("/api/products/brief", String.class);
		
		Map<String, Object> jsonFromFirstResult = jsonToListOfMap(result.getBody()).get(0);
		
		Assert.assertEquals(HttpStatus.OK, result.getStatusCode());
		Assert.assertFalse(jsonFromFirstResult.containsKey("products"));
		Assert.assertFalse(jsonFromFirstResult.containsKey("images"));
	}
	
	@Test
	public void mustFindAProductById() {
		Product expectedProduct = productRepository.findAll().iterator().next();
		Integer id = expectedProduct.getId();
		
		ResponseEntity<String> result = restTemplate.getForEntity("/api/products/" + id, String.class);
		Map<String, Object> product = jsonToMap(result.getBody());
		
		Assert.assertEquals(HttpStatus.OK, result.getStatusCode());
		Assert.assertEquals(id, product.get("id"));
		Assert.assertEquals(expectedProduct.getDescription(), product.get("description"));
	}
	
	@Test
	public void mustFindAProductAsBriefById() throws JsonParseException, JsonMappingException, IOException {
		Product firstProduct = productRepository.findAll().iterator().next();
		Integer id = firstProduct.getId();
		
		ResponseEntity<String> result = restTemplate.getForEntity("/api/products/" + id + "/brief", String.class);
		Map<String, Object> productAsBrief = jsonToMap(result.getBody());
		
		
		Assert.assertEquals(HttpStatus.OK, result.getStatusCode());
		Assert.assertFalse(productAsBrief.containsKey("products"));
		Assert.assertFalse(productAsBrief.containsKey("images"));
	}
	
	@Test
	public void mustFindAllImagesByProduct() {
		Product firstProduct = productRepository.findAll().iterator().next();
		Integer id = firstProduct.getId();
		
		Image expectedImage = firstProduct.getImages().iterator().next();
		
		ResponseEntity<String> result = restTemplate.getForEntity("/api/products/" + id + "/images", String.class);
		Map<String, Object> firstImage = jsonToListOfMap(result.getBody()).get(0);
		
		Assert.assertEquals(HttpStatus.OK, result.getStatusCode());
		Assert.assertEquals(expectedImage.getId(), firstImage.get("id"));
		Assert.assertEquals(expectedImage.getType(), firstImage.get("type"));
	}
	
	@Test
	public void mustFindChildProductsByParentProduct() throws JsonParseException, JsonMappingException, IOException {
		Product firstProduct = productRepository.findAll().iterator().next();
		Integer id = firstProduct.getId();
		
		Product expectedChild = firstProduct.getProducts().iterator().next();
		
		ResponseEntity<String> result = restTemplate.getForEntity("/api/products/" + id + "/children", String.class);
		Map<String, Object> firstChild = jsonToListOfMap(result.getBody()).get(0);
		
		Assert.assertEquals(HttpStatus.OK, result.getStatusCode());
		Assert.assertEquals(expectedChild.getId(), firstChild.get("id"));
		Assert.assertEquals(expectedChild.getDescription(), firstChild.get("description"));
	}
	
	@Transactional
	private void loadInitialData() {
		Product parentProduct = new Product("Parent product");
		
		Product childProduct = new Product("Child product");
		parentProduct.addProduct(childProduct);
		
		Image image = new Image("JPEG");
		parentProduct.addImage(image);
		
		productRepository.save(parentProduct);
		initialDataLoaded = true;
	}
	
	List<Map<String, Object>> jsonToListOfMap(String json) {
		TypeReference<List<Map<String, Object>>> resultType = new TypeReference<List<Map<String, Object>>>() {};
		List<Map<String, Object>> result = null;
		
		try {
			result = new ObjectMapper().readValue(json, resultType);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	Map<String, Object> jsonToMap(String json) {
		TypeReference<Map<String, Object>> resultType = new TypeReference<Map<String, Object>>() {};
		Map<String, Object> result = null;
		
		try {
			result = new ObjectMapper().readValue(json, resultType);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return result;
	}

}
