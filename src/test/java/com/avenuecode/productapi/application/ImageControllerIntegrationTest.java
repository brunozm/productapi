package com.avenuecode.productapi.application;

import javax.transaction.Transactional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.avenuecode.productapi.domain.image.Image;
import com.avenuecode.productapi.domain.image.ImageRepository;
import com.avenuecode.productapi.domain.product.Product;
import com.avenuecode.productapi.domain.product.ProductRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:test-application.properties")
public class ImageControllerIntegrationTest {

	@Autowired
	private TestRestTemplate restTemplate;

	@Autowired
	private ImageRepository imageRepository;

	@Autowired
	private ProductRepository productRepository;
	
	private Boolean initialDataLoaded = false;
	
	@Before
	public void init() {
		if (!initialDataLoaded) {
			loadInitialData();
		}
	}
	
	@Test
	public void mustUpdateAImageById() {
		Image firstImage = imageRepository.findAll().iterator().next();
		Integer id = firstImage.getId();
		
		Image image = new Image("GIF");
		restTemplate.put("/api/images/" + id, image);
		
		Image result = imageRepository.findOne(id);
		
		Assert.assertEquals(image.getType(), result.getType());
	}
	
	@Test
	public void mustDeleteAProductById() {
		Image firstImage = imageRepository.findAll().iterator().next();
		Integer id = firstImage.getId();
		
		restTemplate.delete("/api/images/" + id);
		
		Image result = imageRepository.findOne(id);
		Assert.assertNull(result);
	}

	@Transactional
	private void loadInitialData() {
		Product parentProduct = new Product("Parent product");
		
		Image image = new Image("JPEG");
		parentProduct.addImage(image);

		productRepository.save(parentProduct);
		initialDataLoaded = true;
	}

}
