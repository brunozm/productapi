package com.avenuecode.productapi.product;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.avenuecode.productapi.domain.image.Image;
import com.avenuecode.productapi.domain.image.ImageRepository;
import com.avenuecode.productapi.domain.product.Product;
import com.avenuecode.productapi.domain.product.ProductBrief;
import com.avenuecode.productapi.domain.product.ProductRepository;
import com.avenuecode.productapi.domain.product.ProductService;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceTest {

	@Mock
	private ProductRepository productRepository;
	@Mock
	private ImageRepository imageRepository;

	private ProductService service;
	
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		initMocks();
		
		service = new ProductService(productRepository, imageRepository);
	}

	@Test
	public void mustReturnAListOfProducts() {
		List<Product> result = service.findAll().getFullResult();
		Assert.assertTrue(!result.isEmpty());
	}
	
	@Test
	public void mustReturnAListOfProductsAsBrief() {
		List<Product> result = service.findAll().getFullResult();
		Assert.assertTrue(!result.isEmpty());
	}
	
	@Test
	public void mustReturnAProductById() {
		Product result = service.findBy(1).getFullResult();
		Assert.assertNotNull(result.getDescription());
	}
	
	@Test
	public void mustReturnAProductAsBriefById() {
		ProductBrief result = service.findBy(1).getBriefResult();
		Assert.assertNotNull(result.getDescription());
	}
	
	private void initMocks() {

		Product parentProduct = new Product("Parent product");

		Product childProduct = new Product("Child product");
		parentProduct.addProduct(childProduct);

		Image image = new Image("JPEG");
		parentProduct.addImage(image);


		Mockito.when(productRepository.findByParentNull()).thenReturn(Arrays.asList(parentProduct));
		Mockito.when(productRepository.findOne(Mockito.anyInt())).thenReturn(parentProduct);
	}

}
