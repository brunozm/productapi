package com.avenuecode.productapi.application;

import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.avenuecode.productapi.domain.image.Image;
import com.avenuecode.productapi.domain.image.ImageService;
import com.avenuecode.productapi.domain.product.Product;
import com.avenuecode.productapi.domain.product.ProductBrief;
import com.avenuecode.productapi.domain.product.ProductService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api
@Controller
@Path("/products")
public class ProductController {
	
	private ProductService productService;
	private ImageService imageService;
	
	@Inject
	public ProductController(ProductService productService, ImageService imageService) {
		this.productService = productService;
		this.imageService = imageService;
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@ApiOperation(value = "Finds all products, including child products and images.")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Product> findAll() {
		return productService.findAll().getFullResult();
	}
	
	@ApiOperation(value = "Finds all products, excluding child products and images.")
	@GET
	@Path("/brief")
	@Produces(MediaType.APPLICATION_JSON)
	public List<ProductBrief> findAllBrief() {
		return productService.findAll().getBriefResult();
	}
	
	@ApiOperation(value = "Finds a product by the given id, including child products and images.")
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Product findBy(@PathParam("id") Integer id) {
		return productService.findBy(id).getFullResult();
	}
	
	@ApiOperation(value = "Finds a product by the given id, excluding child products and images.")
	@GET
	@Path("/{id}/brief")
	@Produces(MediaType.APPLICATION_JSON)
	public ProductBrief findBriefBy(@PathParam("id") Integer id) {
		return productService.findBy(id).getBriefResult();
	}
	
	@ApiOperation(value = "Returns a list of child products by the given parent id.")
	@GET
	@Path("/{id}/children")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Product> findChildProductsBy(@PathParam("id") Integer id) {
		return productService.findChildProductsBy(id);
	}
	
	@ApiOperation(value = "Save a new product.")
	@POST
	@Transactional
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Product save(Product product) {
		return productService.save(product);
	}
	
	@ApiOperation(value = "Updates a product by the given id, this service can add and remove child products and images")
	@PUT
	@Path("/{id}")
	@Transactional
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Product update(Product product, @PathParam("id") Integer id) {
		return productService.update(id, product);
	}
	
	@ApiOperation(value = "Deletes a product by the given id.")
	@DELETE
	@Path("/{id}")
	@Transactional
	public void delete(@PathParam("id") Integer id) {
		productService.delete(id);
	}
	
	@ApiOperation(value = "Returns a list of images by the given product id.")
	@GET
	@Path("/{id}/images")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Image> findImagesBy(@PathParam("id") Integer id) {
		return imageService.forProduct(id).findAllImages();
	}
	
	@ApiOperation(value = "Saves an image by the given product id.")
	@POST
	@Path("/{id}/images")
	@Transactional
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Image save(@PathParam("id") Integer id, Image image) {
		return imageService.forProduct(id).save(image);
	}
	
}
