package com.avenuecode.productapi.application;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.stereotype.Controller;

import com.avenuecode.productapi.domain.image.Image;
import com.avenuecode.productapi.domain.image.ImageService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api
@Controller
@Path("/images")
public class ImageController {
	
	private ImageService imageService;

	@Inject
	public ImageController(ImageService imageService) {
		this.imageService = imageService;
	}

	@ApiOperation(value = "Updates an image by the given id")
	@PUT
	@Path("/{id}")
	@Transactional
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Image update(Image image, @PathParam("id") Integer id) {
		return imageService.update(id, image);
	}
	
	@ApiOperation(value = "Delete an image by the given id.")
	@DELETE
	@Path("/{id}")
	@Transactional
	public void delete(@PathParam("id") Integer id) {
		imageService.delete(id);
	}

}
