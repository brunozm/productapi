package com.avenuecode.productapi;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

import com.avenuecode.productapi.application.ImageController;
import com.avenuecode.productapi.application.ProductController;

import io.swagger.jaxrs.listing.ApiListingResource;
import io.swagger.jaxrs.listing.SwaggerSerializers;

@Configuration
@ApplicationPath("/api")
public class JerseyConfig extends ResourceConfig {

	public JerseyConfig() {
		register(ProductController.class);
		register(ImageController.class);
		register(ApiListingResource.class);
		register(SwaggerSerializers.class);
		register(CORSFilter.class);
	}
	
}
