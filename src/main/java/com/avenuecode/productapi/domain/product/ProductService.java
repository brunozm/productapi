package com.avenuecode.productapi.domain.product;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.avenuecode.productapi.domain.image.Image;
import com.avenuecode.productapi.domain.image.ImageRepository;

@Service
public class ProductService {
	
	private ProductRepository productRepository;
	private ImageRepository imageRepository;
	
	@Inject
	public ProductService(ProductRepository productRepository, ImageRepository imageRepository) {
		this.productRepository = productRepository;
		this.imageRepository = imageRepository;
	}

	public ViewListResult findAll() {
		return new ViewListResult(productRepository.findByParentNull());
	}
	
	public ViewSingleResult findBy(Integer id) {
		Product product = productRepository.findOne(id);
		
		if (product == null) {
			throw new ProductNotFoundException("Product with id: " + id + " was not found.");
		}
		
		return new ViewSingleResult(product);
	}
	
	public List<Product> findChildProductsBy(Integer id) {
		return productRepository.findChildrenProductsBy(id);
	}
	
	public Product save(Product product) {
		return productRepository.save(product);
	}
	
	public Product update(Integer id, Product product) {
		Product productPersisted = productRepository.findOne(id);
		
		if (productPersisted == null) {
			throw new ProductNotFoundException("Product with id: " + id + " was not found.");
		}
		
		productPersisted.setDescription(product.getDescription());
		productPersisted.setParent(product.getParent());
		
			
		mergeProducts(productPersisted, product.getProducts()); 
		mergeImages(productPersisted, product.getImages());
		
		return productPersisted;
	}
	
	public void delete(Integer id) {
		Product product = productRepository.findOne(id);
		
		if (product == null) {
			throw new ProductNotFoundException("Product with id: " + id + " was not found.");
		}
		
		delete(product);
	}
	
	private void mergeProducts(Product productPersisted, Set<Product> newProducts) {
		Set<Product> productsToBeRemoved = productPersisted
				.getProducts()
				.stream()
				.filter(p -> !newProducts.contains(p))
				.collect(Collectors.toSet());
		
		productPersisted.removeProducts(productsToBeRemoved);
		
		newProducts.stream()
				.filter(p -> p.getId() == null)
				.forEach(p -> productPersisted.addProduct(productRepository.save(p)));
	}
	
	private void mergeImages(Product productPersisted, Set<Image> newImages) {
		Set<Image> imagesToBeRemoved = productPersisted
				.getImages()
				.stream()
				.filter(image -> !newImages.contains(image))
				.collect(Collectors.toSet());
		
		productPersisted.removeImages(imagesToBeRemoved);
		
		newImages.stream()
				.filter(image -> image.getId() == null)
				.forEach(image -> productPersisted.addImage(imageRepository.save(image)));
	}
	
	private void delete(Product product) {
		productRepository.delete(product);
	}
	
	public class ViewListResult {
		
		private List<Product> products;
		
		public ViewListResult(List<Product> products) {
			this.products = products;
		}

		public List<Product> getFullResult() {
			return products;
		}
		
		public List<ProductBrief> getBriefResult() {
			List<ProductBrief> result = new ArrayList<>();
			products.forEach(product -> result.add(new ProductBrief(product)));
			return result;
		}
	}
	
	public class ViewSingleResult {
		
		private Product product;

		public ViewSingleResult(Product product) {
			this.product = product;
		}
		
		public Product getFullResult() {
			return product;
		}
		
		public ProductBrief getBriefResult() {
			return new ProductBrief(product);
		}
		
	}

}
