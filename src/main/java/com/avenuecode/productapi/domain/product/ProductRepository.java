package com.avenuecode.productapi.domain.product;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface ProductRepository extends CrudRepository<Product, Integer> {
	
	List<Product> findByParentNull();
	
	@Query("select p.products from #{#entityName} p where p.id = :id")
	List<Product> findChildrenProductsBy(@Param("id") Integer id);

}
