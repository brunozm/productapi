package com.avenuecode.productapi.domain.product;

public class ProductNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -1543494361954875970L;

	public ProductNotFoundException(String message) {
		super(message);
	}

}
