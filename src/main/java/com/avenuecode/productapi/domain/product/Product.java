package com.avenuecode.productapi.domain.product;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.avenuecode.productapi.domain.image.Image;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
public class Product {
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	@Column
	private String description;
	
	@ManyToOne
	@JsonProperty(access = Access.WRITE_ONLY)
	private Product parent;
	
	@OneToMany(mappedBy = "parent", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<Product> products = new HashSet<>();
	
	@OneToMany(mappedBy = "product", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<Image> images = new HashSet<>();
	
	
	protected Product() {}

	public Product(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Product getParent() {
		return parent;
	}

	public void setParent(Product parent) {
		this.parent = parent;
	}

	public Integer getId() {
		return id;
	}

	public Set<Product> getProducts() {
		return Collections.unmodifiableSet(products);
	}

	public Set<Image> getImages() {
		return Collections.unmodifiableSet(images);
	}
	
	public void addProduct(Product product) {
		product.setParent(this);
		products.add(product);
	}
	
	public void addProducts(Set<Product> products) {
		products.forEach(product -> addProduct(product));
	}
	
	public void removeProduct(Product product) {
		products.remove(product);
	}
	
	public void removeProducts(Set<Product> products) {
		products.forEach(product -> removeProduct(product));
	}

	public void removeImages(Set<Image> images) {
		images.forEach(image -> removeImage(image));
	}
	
	public void removeImage(Image image) {
		images.remove(image);
	}
	
	public void addImage(Image image) {
		image.setProduct(this);
		images.add(image);
	}
	
	public void addImages(Set<Image> images) {
		images.forEach(image -> addImage(image));
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}



}
