package com.avenuecode.productapi.domain.product;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(value={"products", "images"})
public class ProductBrief extends Product {
	
	@JsonIgnore
	private Product product;
	
	public ProductBrief(Product product) {
		super(product.getDescription());
		this.product = product;
	}

	@Override
	public Integer getId() {
		return product.getId();
	}
	
}
