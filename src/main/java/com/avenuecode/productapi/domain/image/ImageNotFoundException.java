package com.avenuecode.productapi.domain.image;

public class ImageNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -5563773508397360313L;

	public ImageNotFoundException(String message) {
		super(message);
	}

}
