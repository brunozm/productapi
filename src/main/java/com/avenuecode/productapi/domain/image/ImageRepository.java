package com.avenuecode.productapi.domain.image;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface ImageRepository extends CrudRepository<Image, Integer> {
	
	List<Image> findByProductId(Integer id);

}
