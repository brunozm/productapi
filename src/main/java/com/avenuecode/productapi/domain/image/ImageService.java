package com.avenuecode.productapi.domain.image;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.avenuecode.productapi.domain.product.Product;
import com.avenuecode.productapi.domain.product.ProductRepository;

@Service
public class ImageService {

	private ImageRepository imageRepository;
	private ProductRepository productRepository;

	@Inject
	public ImageService(ImageRepository imageRepository, ProductRepository productRepository) {
		this.imageRepository = imageRepository;
		this.productRepository = productRepository;
	}

	public OperationsForProduct forProduct(Integer id) {
		return new OperationsForProduct(id);
	}

	public Image update(Integer id, Image image) {
		Image imagePersisted = imageRepository.findOne(id);

		if (imagePersisted == null) {
			throw new ImageNotFoundException("Image with id: " + id + " was not found.");
		}

		imagePersisted.setType(image.getType());

		return imagePersisted;
	}

	public void delete(Integer id) {
		Image imagePersisted = imageRepository.findOne(id);

		if (imagePersisted == null) {
			throw new ImageNotFoundException("Image with id: " + id + " was not found.");
		}
		
		imageRepository.delete(imagePersisted);
	}

	public class OperationsForProduct {

		private Integer id;

		public OperationsForProduct(Integer id) {
			this.id = id;
		}

		public Image save(Image image) {
			Product product = productRepository.findOne(id);
			image.setProduct(product);
			return imageRepository.save(image);
		}

		public List<Image> findAllImages() {
			return imageRepository.findByProductId(id);
		}

	}

}
